<?php
/**
 * Plugin Name: Author Privacy Policy
 */

define( 'AUTHOR_PRIVACY_POLICY_PLUGIN_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
define( 'AUTHOR_PRIVACY_POLICY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

class Author_Privacy_Policy {
	public function init() {
		load_plugin_textdomain( 'author-privacy-policy', false, dirname( plugin_basename( __FILE__ ) ) );
		if ( is_admin() ) {
			// @todo if is user-edit.php
			wp_register_script( 'author-privacy-policy', AUTHOR_PRIVACY_POLICY_PLUGIN_URL . '/form.js', array(), filemtime( AUTHOR_PRIVACY_POLICY_PLUGIN_PATH . '/form.js' ), true );
			wp_enqueue_script( 'author-privacy-policy' );
		}
		add_action( 'show_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 999 );
		add_action( 'edit_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 999 );
		add_action( 'personal_options_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
		add_action( 'edit_user_profile_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
		add_action( 'user_profile_update_errors', array( __CLASS__, 'validate_extra_user_profile_fields' ), 10, 3 );
	}

	public static function extra_user_profile_fields( WP_User $user ) {
		if ( in_array( 'author', $user->roles, true ) ) {
			?>
			<h3><?php esc_html_e( 'Privacy Policy', 'author-privacy-policy' ); ?></h3>
			<p><input type="checkbox" id="author-privacy-policy" name="author-privacy-policy" <?php echo checked( 1, get_user_meta( $user->id, 'accepted_terms_and_conditions' )[0] ); ?> required/>
			<?php
			// translators: %1$s and %2$s are link tags.
			echo '<label for="author-privacy-policy">' . wp_kses_post( sprintf( __( 'I have read an accept the %1$sPrivacy Policy%2$s.', 'author-privacy-policy' ), '<a href="' . esc_url( get_permalink( 3 ) ) . '" target="_blank">', '</a>' ) ) . '</label>';
			?>
			</p>
			<?php
		}
	}

	public static function save_extra_user_profile_fields( int $user_id ) {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user_id ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		$user = get_user_by( 'ID', $user_id );

		if ( in_array( 'author', $user->roles, true ) ) {
			if ( array_key_exists( 'author-privacy-policy', $_POST ) && 'on' === $_POST['author-privacy-policy'] ) {
				update_user_meta( $user_id, 'accepted_terms_and_conditions', true );
			} else {
				update_user_meta( $user_id, 'accepted_terms_and_conditions', false );
			}
		}
	}

	public static function validate_extra_user_profile_fields( \WP_Error $errors, bool $update, stdClass $user ): void {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user->ID ) ) {
			return;
		}

		if ( 'author' === $user->role ) {
			if ( ! array_key_exists( 'author-privacy-policy', $_POST ) || 'on' !== $_POST['author-privacy-policy'] ) {
				$errors->add( 'author_privacy_policy', esc_html__( 'You need to accept the privacy policy.', 'author-social-networks' ) );
			}
		}
	}
}

$author_social_networks = new Author_Privacy_Policy();
add_action( 'init', array( $author_social_networks, 'init' ) );
