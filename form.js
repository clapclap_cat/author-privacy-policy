const checkbox = document.querySelector("#author-privacy-policy");
const submit = document.querySelector("#submit");
if (!checkbox.checked) {
  submit.setAttribute("disabled", "true");
}
checkbox.addEventListener("change", (e) => {
  if (!checkbox.checked) {
    submit.setAttribute("disabled", "true");
  } else {
    submit.removeAttribute("disabled");
  }
});
